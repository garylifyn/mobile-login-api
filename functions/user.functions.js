const jsonwebtoken = require('jsonwebtoken');
const got = require('got');

const firebaseAdmin = require('../config/firebase');

const User = require('../models').User;

// Get JWT token
function getJwt(user_id) {
  const expiresIn = process.env.JWT_EXPIRES;

  const payload = {
    sub: user_id,
    issue_at: Date.now(),
  };

  const signToken = jsonwebtoken.sign(payload, process.env.JWT_SECRET, {
    expiresIn,
  });

  return {
    token: 'Bearer ' + signToken,
    expires: expiresIn,
  };
}

module.exports.checkUserPhone = (res, body) => {
  return User.findOne({ where: { phone_number: body.phone_number } })
    .then((user) => {
      if (user) {
        return successResponse(res, '', { ...user.toJson() });
      }

      return successResponse(res, '', body.phone_number);
    })
    .catch((err) => Promise.reject(err));
};

module.exports.mobileLogin = (res, body) => {
  return firebaseAdmin
    .auth()
    .verifyIdToken(body.idToken)
    .then((decodedToken) => {
      const uid = decodedToken.uid;
      const phoneNumber = decodedToken.phone_number;

      return User.findOrCreate({
        where: { phone_number: phoneNumber },
      })
        .then(([user, created]) => {
          if (created) {
            const jwtToken = getJwt(user.id);
            const data = {
              ...user.toJson(),
              token: jwtToken.token,
              expires_in: jwtToken.expires,
            };

            // console.log(data);

            return successResponse(res, 'Phone number created', data);
          }

          const jwtToken = getJwt(user.id);
          const data = {
            ...user.toJson(),
            token: jwtToken.token,
            expires_in: jwtToken.expires,
          };

          return successResponse(res, 'It existed', data);
        })
        .catch((err) => Promise.reject(err));
    })
    .catch((err) => Promise.reject(err));
};

module.exports.updateUserDetails = (res, user) => {
  return user
    .save()
    .then((updatedUser) => {
      const data = {
        id: updatedUser.id,
        name: updatedUser.name,
        email: updatedUser.email,
        phone_number: updatedUser.phone_number,
        profile_picture_url: updatedUser.profile_picture,
      };

      return successResponse(res, 'User details updated', data);
    })
    .catch((err) => Promise.reject(err));
};

module.exports.updateUserWithGoogle = (res, user, body) => {
  const accessToken = body.google_access_token;

  // Similar to request npm package
  return got(
    `https://www.googleapis.com/oauth2/v1/userinfo?alt=json&access_token=${accessToken}`
  )
    .then((respond) => {
      // console.log(JSON.parse(respond.body));

      const result = JSON.parse(respond.body);

      return user
        .update({
          id: user.id,
          name: result.name,
          email: result.email,
          phone_number: user.phone_number,
          google_id: result.id,
          profile_picture_url: result.picture,
        })
        .then((updatedUser) => {
          // console.log(updatedUser.toJson());

          const data = {
            data: {
              ...updatedUser.toJson(),
            },
          };

          console.log(data);

          return successResponse(res, 'GOOGLE SIGNED IN!!!!', data);
        })
        .catch((err) => Promise.reject(err));
    })
    .catch((err) => Promise.reject(err));
};

module.exports.updateUserWithFacebook = (res, user, body) => {
  const accessToken = body.facebook_access_token;

  return got(
    `https://graph.facebook.com/me?fields=id,name,email,picture.height(200)&access_token=${accessToken}`
  )
    .then((respond) => {
      // console.log(JSON.parse(respond.body));

      const result = JSON.parse(respond.body);

      return user
        .update({
          id: user.id,
          name: result.name,
          email: result.email,
          phone_number: user.phone_number,
          facebook_id: result.id,
          profile_picture_url: result.picture.data.url,
        })
        .then((updatedUser) => {
          // console.log(updatedUser.toJson());

          const data = {
            data: {
              ...updatedUser.toJson(),
            },
          };

          console.log(data);

          return successResponse(res, 'FACEBOOK SIGNED IN!!!!', data);
        })
        .catch((err) => Promise.reject(err));
    })
    .catch((err) => {
      console.log(err);
      Promise.reject(err);
    });
};
