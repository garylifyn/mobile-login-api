successResponse = (res, message, data) => {
  const success = { success: true, message };

  return res.json({ data, ...success });
};

errorResponse = (res, message, statusCode) => {
  const error = { success: false, message };

  if (!statusCode || typeof statusCode === 'undefined') {
    console.error('Error status code MUST BE ASSIGNED');
  }

  res.statusCode = statusCode;

  return res.status(statusCode).json(error);
};
