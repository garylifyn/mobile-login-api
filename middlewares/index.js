const passport = require('passport');

// Check the JWT
module.exports.validateJwt = (req, res, next) => {
  passport.authenticate('jwt', { session: false }, (err, user, info) => {
    if (!user) {
      console.log(info);
      next(err);
    }

    if (err) {
      next(err);
    }

    req.user = user;

    next();
  })(req, res, next);
};

// For route api not found
module.exports.routeNotFound = (req, res, next) => {
  const error = new Error(`404 Not Found - ${req.originalUrl}`);
  res.status(404);
  next(error);
};

// For handling error such as route not found, bad request, database error, etc.
module.exports.errorHandler = (err, req, res, next) => {
  const errorMessage =
    process.env.NODE_ENV === 'production'
      ? 'Oops, Error Occured 🤔'
      : err.message;

  console.log(errorMessage);

  res.status(err.statusCode || 500);
  res.json({ success: false, message: errorMessage });
};
