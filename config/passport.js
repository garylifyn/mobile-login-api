const JwtStrategy = require('passport-jwt').Strategy;
const ExtractJwt = require('passport-jwt').ExtractJwt;

const User = require('../models').User;

const options = {
  jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
  secretOrKey: process.env.JWT_SECRET,
};

const jwtStrategy = new JwtStrategy(options, (jwt_payload, done) => {
  User.findByPk(jwt_payload.sub)
    .then((user) => {
      if (user) {
        return done(null, user);
      }

      return done('Invalid token', false);
    })
    .catch((err) => done('No user found!', null));
});

module.exports = (passport) => {
  passport.use(jwtStrategy);
};
