const firebaseAdmin = require('firebase-admin');

const serviceAccount = require('./mobile-login-e086c-firebase-adminsdk-6qeok-43e07f81c6.json');

firebaseAdmin.initializeApp({
  credential: firebaseAdmin.credential.cert(serviceAccount),
});

module.exports = firebaseAdmin;
