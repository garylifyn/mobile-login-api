const express = require('express');
const helmet = require('helmet');
const morgan = require('morgan');
const passport = require('passport');
const cors = require('cors');

// Global functions
require('./global_functions.js');

const db = require('./models');
const allRoutes = require('./routes/');
const { routeNotFound, errorHandler } = require('./middlewares');

// Pass global passport into the configure passport function
require('./config/passport.js')(passport);

const app = express();

app.use(helmet());
app.use(cors());
app.use(passport.initialize());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

if (process.env.NODE_ENV !== 'production') {
  app.use(morgan('dev'));
}

// All routes
app.use(allRoutes);

// Error Handling
app.use(routeNotFound);
app.use(errorHandler);

const port = 3000;
app.listen(port, async () => {
  try {
    // Connecting to database and Create tables if existed
    await db.sequelize.sync();
    console.log('Database connected & Tables created successfully');

    console.log(`Listening on port ${port}`);
  } catch (err) {
    console.error(err);
  }
});
