const { body, validationResult } = require('express-validator');

const {
  checkUserPhone,
  mobileLogin,
  updateUserDetails,
  updateUserWithGoogle,
  updateUserWithFacebook,
} = require('../../functions/user.functions.js');

module.exports.checkUserPhone = [
  body('phone_number')
    .notEmpty()
    .withMessage('phone number cannot be empty')
    .isMobilePhone(['ms-MY', 'en-SG'], { strictMode: true }) // strictMode ensure phone no. begin with +60
    .withMessage('Must be valid phone number'),
  (req, res, next) => {
    // check request body errors
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
      return errorResponse(res, errors.array(), 400);
    }

    return checkUserPhone(res, req.body);
  },
];

module.exports.userMobileLogin = [
  body('idToken').notEmpty().withMessage('ID token cannot be empty'),
  (req, res, next) => {
    // check request body errors
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
      return errorResponse(res, errors.array(), 400);
    }

    // user.functions: using mobile login
    return mobileLogin(res, req.body);
  },
];

module.exports.getUserDetails = (req, res, next) => {
  res.json({
    data: {
      ...req.user.toJson(),
    },
    success: true,
    message: '',
  });
};

module.exports.updateUser = [
  body('name')
    .isLength({ min: 5, max: 20 })
    .optional({ nullable: true, checkFalsy: true })
    .withMessage('Name must be minimum 3 and maximum 20 characters'),
  body('email')
    .isEmail()
    .optional({ nullable: true, checkFalsy: true })
    .withMessage('Invalid email'),
  body('phone_number')
    .isMobilePhone(['ms-MY', 'en-SG'], { strictMode: true })
    .optional({ nullable: true, checkFalsy: true })
    .withMessage('Must be valid phone number'),
  body('profile_picture_url')
    .isString()
    .optional({ nullable: true, checkFalsy: true }),
  (req, res, next) => {
    const user = req.user;

    const errors = validationResult(req);

    if (!errors.isEmpty()) {
      return errorResponse(res, errors.array(), 400);
    }

    // Compare between body request and user details
    user.name = req.body.name === '' ? user.name : req.body.name;

    user.email = req.body.email === '' ? user.email : req.body.email;

    user.phone_number =
      req.body.phone_number === '' ? user.phone_number : req.body.phone_number;

    user.profile_picture_url =
      req.body.profile_picture_url === ''
        ? user.profile_picture_url
        : req.body.profile_picture_url;

    // user.functions: update user details
    return updateUserDetails(res, user);
  },
];

module.exports.updateUserWithGoogle = [
  body('google_access_token')
    .optional()
    .notEmpty()
    .withMessage('Google access token is required'),
  (req, res, next) => {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
      return errorResponse(res, errors.array(), 400);
    }

    return updateUserWithGoogle(res, req.user, req.body);
  },
];

module.exports.updateUserWithFacebook = [
  body('facebook_access_token')
    .optional()
    .notEmpty()
    .withMessage('Facebook access token is required'),
  (req, res, next) => {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
      return errorResponse(res, errors.array(), 400);
    }

    return updateUserWithFacebook(res, req.user, req.body);
  },
];
