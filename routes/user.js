const express = require('express');

const { validateJwt } = require('../middlewares');
const userController = require('./controller/user.controller.js');

const router = express.Router();

/**
 * @api {post} /user/checkUserPhone Check User phone number is exist or not
 * @apiName CheckUserPhone
 * @apiGroup User
 *
 * @apiParam {String} phone_number Phone number of the User.
 * @apiParamExample {json} Request-Example:
 *     {
 *       "phone_number": "+60112380554"
 *     }
 *
 * @apiSuccess {String} data User Data
 * @apiSuccess {String} data.user_id User ID
 * @apiSuccess {String} data.name User Name
 * @apiSuccess {String} data.email User Email
 * @apiSuccess {String} data.phone_number User Phone number
 * @apiSuccess {String} data.facebook_id User facebook ID
 * @apiSuccess {String} data.google_id User google ID
 * @apiSuccess {String} data.profile_picture_url User Profile picture URL
 * @apiSuccess {boolean} success true or false
 * @apiSuccess {String} message Return Message.
 *
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *        "data": {
 *            "user_id": 11,
 *            "name": "Jeff Migga",
 *            "email": "jeffmigga11@gmail.com",
 *            "phone_number": "+60123375483",
 *            "faceboook_id": null,
 *            "google_id": "106302447108605690375"
 *        },
 *        "success": true,
 *        "message": ""
 *     }
 *
 * @apiSuccess {String} data Phone number
 * @apiSuccess {boolean} success true or false
 * @apiSuccess {String} message return message
 *
 * @apiSuccessExample {json} Success-Response:
 *    HTTP/1.1 200 OK
 *    {
 *       "data": "+60123375480",
 *       "success": true,
 *       "message": ""
 *    }
 */
router.post('/checkUserPhone', userController.checkUserPhone);

/**
 * @api {post} /user/login Login User with phone number
 * @apiName LoginUser
 * @apiGroup User
 *
 * @apiParam {String} phone_number Phone number of the User.
 * @apiParamExample {json} Request-Example:
 *     {
 *       "phone_number": "+60112380554"
 *     }
 *
 * @apiSuccess {String} data User data
 * @apiSuccess {String} data.user_id User ID
 * @apiSuccess {String} data.name User name
 * @apiSuccess {String} data.email User email
 * @apiSuccess {String} data.phone_number Phone number of the User
 * @apiSuccess {String} data.profile_picture_url User Profile picture URL
 * @apiSuccess {String} data.token User JWT token
 * @apiSuccess {String} data.expires_in Token expires in
 * @apiSuccess {boolean} success true or false
 * @apiSuccess {String} message Return Message.
 *
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "data": {
 *          "user_id": 1,
 *          "name": "Kelly",
 *          "email": "kelly@example.com",
 *          "phone_number": "+60112380554",
 *          "profile_picture": "assets/profile_pictures/thumb_thumb.png",
 *          "token": "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsImlzc3VlX2F0IjoxNTk3MTI3OTU2MzIwLCJpYXQiOjE1OTcxMjc5NTYsImV4cCI6MTU5NzIxNDM1Nn0.C-ZXbgYGE1wymS64T1PllbPvdKGsK7dyjes0PboGBsA",
 *          "expires_in": "1d"
 *        },
 *       "success": true,
 *       "message": "It existed"
 *     }
 *
 * @apiError BadRequest Must be a valid phone number.
 *
 * @apiErrorExample {json} Error-Response (400 - Bad Request):
 *     HTTP/1.1 400 Bad Request
 *     {
 *       "errors": [
 *          {
 *            "value": "1111",
 *            "msg": "Must be valid phone number",
 *            "param": "phone_number",
 *            "location": "body"
 *          }
 *       ]
 *     }
 *
 * @apiError NotFound The phone number of the User was not found.
 *
 * @apiErrorExample {json} Error-Response (404 - Not Found):
 *     HTTP/1.1 404 Not Found
 *     {
 *       "errors": "phone number not existed"
 *     }
 */
router.post('/login', userController.userMobileLogin);

// ------------------------------------------------------
// After this, everything is validate by JWT
// ------------------------------------------------------
router.use(validateJwt);

/**
 * @api {GET} /user Get User Details
 * @apiName GetUser
 * @apiGroup User
 *
 * @apiHeader {String} Content-Type application/x-www-form-urlencoded
 * @apiHeader {String} Authorization token
 *
 * @apiSuccess {String} data User Data
 * @apiSuccess {String} data.user_id User ID
 * @apiSuccess {String} data.name User Name
 * @apiSuccess {String} data.email User Email
 * @apiSuccess {String} data.phone_number User Phone number
 * @apiSuccess {String} data.profile_picture_url User Profile picture URL
 * @apiSuccess {boolean} success true or false
 * @apiSuccess {String} message Return Message.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *        "data": {
 *            "user_id": 2,
 *            "name": "Lee Hiyoshi",
 *            "email": "lee.hiyoshi123@email.com",
 *            "phone_number": "+60181576623",
 *            "profile_picture_url": "assets/profile_pictures/thumb_thumb.png"
 *        },
 *        "success": true,
 *        "message": ""
 *     }
 *
 */
router.get('/', userController.getUserDetails);

/**
 * @api {POST} /user/update Update User Details
 * @apiName UpdateUser
 * @apiGroup User
 *
 * @apiHeader {String} Content-Type application/x-www-form-urlencoded
 * @apiHeader {String} Authorization token
 *
 * @apiParam {String{5..20}} [name] name
 * @apiParam {String} [email] Email
 * @apiParam {String} [phone_number] Phone number of the User.
 * @apiParam {String} [profile_picture_url] Profile image URL of the User.
 *
 * @apiSuccess {String} data User Data
 * @apiSuccess {String} data.user_id User ID
 * @apiSuccess {String} data.name User Name
 * @apiSuccess {String} data.email User Email
 * @apiSuccess {String} data.phone_number User Phone number
 * @apiSuccess {String} data.profile_picture_url User Profile picture URL
 * @apiSuccess {boolean} success true or false
 * @apiSuccess {String} message Return Message.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *      {
 *        "data": {
 *            "user_id": 2,
 *            "name": "Lee Hiyoshi",
 *            "email": "lee.hiyoshi123@email.com",
 *            "phone_number": "+60181576623",
 *            "profile_picture_url": "assets/profile_pictures/thumb_thumb.png"
 *        },
 *        "success": true,
 *        "message": ""
 *      }
 *
 */
router.post('/update', userController.updateUser);

/**
 * @api {POST} /user/updateUserWithGoogle Update User With Google
 * @apiName UpdateUserWithGoogle
 * @apiGroup User
 *
 * @apiHeader {String} Content-Type application/x-www-form-urlencoded
 * @apiHeader {String} Authorization token
 *
 * @apiParam {String} [google_access_token] Google access token
 *
 * @apiSuccess {String} data User Data
 * @apiSuccess {String} data.user_id User ID
 * @apiSuccess {String} data.name User Name
 * @apiSuccess {String} data.email User Email
 * @apiSuccess {String} data.phone_number User Phone number
 * @apiSuccess {String} data.facebook_id User facebook ID
 * @apiSuccess {String} data.google_id User google ID
 * @apiSuccess {String} data.profile_picture_url User Profile picture URL
 * @apiSuccess {boolean} success true or false
 * @apiSuccess {String} message Return Message.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *      {
 *        "data": {
 *          "user_id": 11,
 *          "name": "Jeff Migga",
 *          "email": "jeffmigga11@gmail.com",
 *          "phone_number": "+60123375483",
 *          "facebook_id": null,
 *          "google_id": 106302447108605690375
 *          "profile_picture": "https://lh5.googleusercontent.com/-Nfu4m-bo8T0/AAAAAAAAAAI/AAAAAAAAAAA/AMZuuclQexskmI6h2cSiva-kPfWW0TQ2CA/photo.jpg"
 *        },
 *        "success": true,
 *        "message": "GOOGLE SIGNED IN!!!!"
 *      }
 *
 */
router.post('/updateUserWithGoogle', userController.updateUserWithGoogle);

/**
 * @api {POST} /user/updateUserWithFacebook Update User With Facebook
 * @apiName UpdateUserWithFacebook
 * @apiGroup User
 *
 * @apiHeader {String} Content-Type application/x-www-form-urlencoded
 * @apiHeader {String} Authorization token
 *
 * @apiParam {String} [facebook_access_token] Facebook access token
 *
 * @apiSuccess {String} data User Data
 * @apiSuccess {String} data.user_id User ID
 * @apiSuccess {String} data.name User Name
 * @apiSuccess {String} data.email User Email
 * @apiSuccess {String} data.phone_number User Phone number
 * @apiSuccess {String} data.facebook_id User facebook ID
 * @apiSuccess {String} data.google_id User google ID
 * @apiSuccess {String} data.profile_picture_url User Profile picture URL
 * @apiSuccess {boolean} success true or false
 * @apiSuccess {String} message Return Message.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *      {
 *        "data": {
 *          "user_id": 10,
 *          "name": "Dev Raymond",
 *          "email": "raymondleeyl@lifyn.co",
 *          "phone_number": "+60112380554",
 *          "facebook_id": null,
 *          "google_id": 106302447108605690375
 *          "profile_picture": "https://scontent.fkul4-2.fna.fbcdn.net/v/t1.30497-1/p200x200/84628273_176159830277856_972693363922829312_n.jpg?_nc_cat=1&_nc_sid=12b3be&_nc_ohc=lORvTk8rvqIAX_22YVW&_nc_ht=scontent.fkul4-2.fna&_nc_tp=6&oh=857f22eed2dbf794f2dde0ad46a552d6&oe=5F648A52"
 *        },
 *        "success": true,
 *        "message": "GOOGLE SIGNED IN!!!!"
 *      }
 *
 */
router.post('/updateUserWithFacebook', userController.updateUserWithFacebook);

module.exports = router;
