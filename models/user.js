const fs = require('fs');

module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define(
    'User',
    {
      name: {
        type: DataTypes.STRING,
        // allowNull: false,
      },
      email: {
        type: DataTypes.STRING,
        // allowNull: false,
        unique: true,
      },
      phone_number: {
        type: DataTypes.STRING,
        allowNull: false,
        unique: true,
      },
      facebook_id: {
        type: DataTypes.STRING,
      },
      google_id: {
        type: DataTypes.STRING,
      },
      profile_picture_url: {
        type: DataTypes.STRING,
      },
      profile_picture: {
        // Sames as getter & setter methods, but this column will not appear in database
        // is encourage to use this since getter & setter methods will likely to be deprecated in the future.
        // For more: https://sequelize.org/master/manual/getters-setters-virtuals.html#virtual-fields
        type: DataTypes.VIRTUAL,
        get() {
          if (this.profile_picture_url) {
            let prefix = '';

            if (process.env.NODE_ENV !== 'production') {
              prefix = 'assets/profile_pictures';
            }

            return `${prefix}/thumb_${this.profile_picture_url}`;
          } else {
            return null;
          }
        },
      },
    },
    {
      freezeTableName: true,
      createdAt: 'user_date',
      updatedAt: 'user_updated_date',
    }
  );

  User.prototype.toJson = function () {
    const json = this.toJSON();

    const userData = {
      user_id: json.id,
      name: json.name,
      email: json.email,
      phone_number: json.phone_number,
      // profile_picture: json.profile_picture,
      faceboook_id: json.facebook_id,
      google_id: json.google_id,
    };

    return userData;
  };

  return User;
};
